<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'model','address','totalPrice','isPaid','telephone','deliveryDate','user_id','status_id','product_id'
    ];

    public function owner(){
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function own(){
        return $this->belongsTo('App\Models\Product','product_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    } 
}
